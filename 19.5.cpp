﻿#include<iostream>
using namespace std;
class Animal {
protected:
    string name;
public:
    Animal(string _name) :name(_name) {}
    virtual void Voice() {
        cout << name << endl;
    };
};
class Dog : public Animal {
public:
    Dog(string _name) :Animal(_name) {};
    void Voice() override {
        Animal::Voice();
        cout << "Woof!" << endl;
    };
};
class Cat : public Animal {
public:
    Cat(string _name) : Animal(_name) {};
    void Voice()  override {
        Animal::Voice();
        cout << "Meow!" << endl;
    };
};
class Cow : public Animal {
public:
    Cow(string _name) :Animal(_name) {};
    void Voice() override {
        Animal::Voice();
        cout << "Mooo!" << endl;
    };
};
int main() {
    Dog dog("Molly");
    Cat cat("Barbie");
    Cow cow("Liza");
    Animal* arr[] = { &dog, &cat, &cow };
    for (int i = 0; i < sizeof(arr) / sizeof(Animal*); i++)
        arr[i]->Voice();
    return 0;
}